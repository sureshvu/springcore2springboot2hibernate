package com.spring.DI.setter;

public class Employee {
	private int empid;
	private String name;
	private float sal;

	public int getEmpid() {
		return empid;
	}

	public void setEmpid(int empid) {
		this.empid = empid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getSal() {
		return sal;
	}

	public void setSal(float sal) {
		this.sal = sal;
	}

	void display() {
		System.out.println(empid + " " + name + " " + sal);
	}

}
