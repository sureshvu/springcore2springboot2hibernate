package com.spring.DI.setter;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Mainapp {
	public static void main(String[] args) {
		/*
		 * Resource r = new ClassPathResource("applicationContext.xml"); BeanFactory
		 * factory = new XmlBeanFactory(r);
		 */
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		Employee e = (Employee) context.getBean("emp");
		e.display();

	}
}
