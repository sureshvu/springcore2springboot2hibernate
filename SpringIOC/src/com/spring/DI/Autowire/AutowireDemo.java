package com.spring.DI.Autowire;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AutowireDemo {
	public static void main(String[] args) {
		// ClassPathXmlApplicationContext context = new
		// ClassPathXmlApplicationContext("autowireconfig.xml");
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("autowireannoconfig.xml");
		IPayService bean = (IPayService) context.getBean("payServiceBean");
		bean.performpayment();
		context.close();
	}
}
