package com.spring.DI.Autowire;

import org.springframework.beans.factory.annotation.Autowired;

public class PaymentServiceImplAnnotation implements IPayService {

	
	private IPayment payment;
	
	private int amount;

	
	@Autowired
	public PaymentServiceImplAnnotation(IPayment payment) {
		
		this.payment = payment;
	}

	@Override
	public void performpayment() {
		// TODO Auto-generated method stub
		payment.executepayment(amount);
	}

	/*
	 * public IPayment getPayment() { return payment; }
	 * 
	 * public void setPayment(IPayment payment) { this.payment = payment; }
	 */

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

}
