package com.spring.DI.Constructor;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
	public static void main(String[] args) {

		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("amountbean.xml");
		AmountBean bean = (AmountBean) context.getBean("amountBean");
		bean.displayValue();
		context.close();
	}
}