package com.spring.DI.Wiring;

import java.util.List;
import java.util.Set;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class WiringApp {
	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("Wiringbean.xml");
		Order orderbean = (Order) context.getBean("orderBean");

		List<String> itemlist = orderbean.getItemList();

		/*
		 * for (String s : itemlist) { System.out.println("Item list----" + s); }
		 */
		Set<String> itemset = orderbean.getItemSet();

		System.out.println("===In java8===Itemset/ItemList");
		System.out.print(" ");
		
		itemlist.stream().forEach(System.out::println);
		
		System.out.print(" ");
		
		itemset.stream().forEach(System.out::println);
	}
}
