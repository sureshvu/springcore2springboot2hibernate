package com.spring.scenarios;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CD1 {
	private CD2 cd2;

	@Autowired
	 CD1(CD2 cd2) {
		
		this.cd2 = cd2;
	}

}
