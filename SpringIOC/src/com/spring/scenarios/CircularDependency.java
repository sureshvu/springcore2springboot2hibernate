package com.spring.scenarios;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

public class CircularDependency {
	public static void main(String[] args) {
		AbstractApplicationContext context=new AnnotationConfigApplicationContext("Config.class");
		/*
		 * CD2 cd2 = (CD2)context.getBean("classA"); cd2.displaymessage();
		 */
	     context.close();
	}
}
