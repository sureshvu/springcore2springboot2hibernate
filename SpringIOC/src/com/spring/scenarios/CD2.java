package com.spring.scenarios;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CD2 {
	
	private CD1 cd1;

	@Autowired
	 CD2(CD1 cd1) {
	
		this.cd1 = cd1;
	}
	
	void displaymessage() {
		System.out.println("In CD2 class");
	}

}
