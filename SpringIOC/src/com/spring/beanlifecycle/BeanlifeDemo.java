package com.spring.beanlifecycle;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BeanlifeDemo {
	public static void main(String[] args) {

		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("BeanLifeCycle.xml");
		IPaymentService bean = (IPaymentService) context.getBean("payServiceBean");
		bean.performpayment();
		context.close();
	}
}