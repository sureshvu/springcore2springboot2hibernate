package com.spring.beanlifecycle;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class PayServiceImpl implements IPaymentService, InitializingBean, DisposableBean {

	private IPayment payment;

	public PayServiceImpl() {

	}

	public PayServiceImpl(IPayment payment) {
		this.payment = payment;
	}

	public void destroy() throws Exception {
		System.out.println("Calling destroy method");
	}

	public void afterPropertiesSet() throws Exception {
		System.out.println("Calling after properties set method");
	}

	public IPayment getPayment() {
		return payment;
	}

	public void setPayment(IPayment payment) {
		this.payment = payment;
	}

	@Override
	public void performpayment() {
		// TODO Auto-generated method stub
		payment.executepayment();
	}

}
