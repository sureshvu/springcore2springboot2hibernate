package com.sss.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.sss.model.Student;
import com.sss.model.StudentLocal;
import com.sss.service.StudentLocalService;

@RestController

public class StudentLocalController {
	@Autowired
	StudentLocalService studentlocalservice;
	String STATUS = "STUDENT LOCAL SAVED";
	StudentLocal studentlocal = new StudentLocal();

	@GetMapping("/studentlocal")
	private String saveStudentlocal() {
		String url = "http://localhost:1212/student/1";
		RestTemplate restTemplate = new RestTemplate();
		Student student = restTemplate.getForObject(url, Student.class);
		studentlocal.setLid(student.getId());
		studentlocal.setLage(student.getAge());
		studentlocal.setLname(student.getName());
		studentlocalservice.saveOrUpdate(studentlocal);
		return STATUS;
	}
}
