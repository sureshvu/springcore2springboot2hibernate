package com.sss.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sss.model.Student;
import com.sss.model.Unit;
import com.sss.service.UnitService;

@RestController
public class UnitController {

	@Autowired
	private UnitService unitservice;
	
	@GetMapping("/unit")
	private List<Unit> getUnitDetails() {
		return unitservice.getUnitDetails();
	}

	@PostMapping("/unit")
	private int saveUnit(@RequestBody Unit unit) {
		unitservice.saveOrUpdate(unit);
		return unit.getValue();
	}
}
