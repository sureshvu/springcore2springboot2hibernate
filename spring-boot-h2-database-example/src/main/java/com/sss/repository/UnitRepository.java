package com.sss.repository;

import org.springframework.data.repository.CrudRepository;

import com.sss.model.Unit;

public interface UnitRepository extends CrudRepository<Unit, Integer> {

}
