package com.sss.repository;
import org.springframework.data.repository.CrudRepository;

import com.sss.model.Student;
public interface StudentRepository extends CrudRepository<Student, Integer>
{
}
