package com.sss.repository;

import org.springframework.data.repository.CrudRepository;

import com.sss.model.StudentLocal;

public interface StudentLocalRepository extends CrudRepository<StudentLocal, Integer>{

}
