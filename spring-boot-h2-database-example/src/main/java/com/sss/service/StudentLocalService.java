package com.sss.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sss.model.StudentLocal;
import com.sss.repository.StudentLocalRepository;

@Service
public class StudentLocalService {
	
	@Autowired
	StudentLocalRepository studentlocalrepository;
	
	public void saveOrUpdate(StudentLocal studentlocal) {
		studentlocalrepository.save(studentlocal);
	}
}
