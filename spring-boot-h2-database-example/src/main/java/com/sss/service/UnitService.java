package com.sss.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sss.model.Student;
import com.sss.model.Unit;
import com.sss.repository.UnitRepository;

@Service
public class UnitService {

	@Autowired
	private UnitRepository unitRepository;

	// getting unit details
	public List<Unit> getUnitDetails() {
		List<Unit> units = new ArrayList<Unit>();
		unitRepository.findAll().forEach(unit -> units.add(unit));
		return units;
	}

	// Save
	public void saveOrUpdate(Unit unit) {
		unitRepository.save(unit);
	}
}
