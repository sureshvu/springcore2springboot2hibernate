package com.sss;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.sss.model.Employee;
import com.sss.model.EmployeeHealthInsurance;
import com.sss.service.EmployeeService;
import com.sss.service.OrganizationService;
import com.sss.service.impl.InvalidInsuranceAmountException;

@SpringBootApplication
public class SpringBootJdbcApplication {

	@Autowired
	EmployeeService employeeService;

	public static void main(String[] args) throws InvalidInsuranceAmountException {
		ApplicationContext context = SpringApplication.run(SpringBootJdbcApplication.class, args);
		OrganizationService organizationService = context.getBean(OrganizationService.class);
		// This service taken for testing Transacion propagation levels
		// EmployeeService employeeService = context.getBean(EmployeeService.class);

		Employee emp = new Employee();
		emp.setEmpId("A100");
		emp.setEmpName("BOB");

		EmployeeHealthInsurance employeeHealthInsurance = new EmployeeHealthInsurance();
		employeeHealthInsurance.setEmpId("A100");
		employeeHealthInsurance.setHealthInsuranceSchemeName("starhealthremium");
		employeeHealthInsurance.setCoverageAmount(-1);

		organizationService.joinOrganization(emp, employeeHealthInsurance);
		// employeeService.insertEmployee(emp);

	}
}