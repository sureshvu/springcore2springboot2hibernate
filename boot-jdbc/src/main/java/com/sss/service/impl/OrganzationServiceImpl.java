package com.sss.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sss.model.Employee;
import com.sss.model.EmployeeHealthInsurance;
import com.sss.service.EmployeeService;
import com.sss.service.HealthInsuranceService;
import com.sss.service.OrganizationService;

@Service
public class OrganzationServiceImpl implements OrganizationService {

	@Autowired
	EmployeeService employeeService;

	@Autowired
	HealthInsuranceService healthInsuranceService;

	@Override
	@Transactional(rollbackFor = InvalidInsuranceAmountException.class)
	public void joinOrganization(Employee employee, EmployeeHealthInsurance employeeHealthInsurance) throws InvalidInsuranceAmountException {
		employeeService.insertEmployee(employee);

		/*
		 * if (employee.getEmpId().equals("A100")) { throw new
		 * RuntimeException("thowing exception to test transaction rollback"); }
		 */

		healthInsuranceService.registerEmployeeHealthInsurance(employeeHealthInsurance);
	}

	@Override

	public void leaveOrganization(Employee employee, EmployeeHealthInsurance employeeHealthInsurance) {
		employeeService.deleteEmployeeById(employee.getEmpId());
		healthInsuranceService.deleteEmployeeHealthInsuranceById(employeeHealthInsurance.getEmpId());
	}
}