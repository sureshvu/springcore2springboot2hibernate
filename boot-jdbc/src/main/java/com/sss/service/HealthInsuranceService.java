package com.sss.service;

import com.sss.model.EmployeeHealthInsurance;
import com.sss.service.impl.InvalidInsuranceAmountException;

public interface HealthInsuranceService {
	void registerEmployeeHealthInsurance(EmployeeHealthInsurance employeeHealthInsurance) throws InvalidInsuranceAmountException;

	void deleteEmployeeHealthInsuranceById(String empid);
}