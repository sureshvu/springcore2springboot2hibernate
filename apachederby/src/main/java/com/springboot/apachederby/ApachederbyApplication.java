package com.springboot.apachederby;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApachederbyApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApachederbyApplication.class, args);
	}

}
