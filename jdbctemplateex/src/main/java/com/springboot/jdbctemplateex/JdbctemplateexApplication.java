package com.springboot.jdbctemplateex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JdbctemplateexApplication {

	public static void main(String[] args) {
		SpringApplication.run(JdbctemplateexApplication.class, args);
	}

}
